(ns closeness-centrality.centrality_test
    (:require [clojure.test :refer :all]
              [closeness-centrality.centrality :refer :all]))

(deftest farness-test
  (testing "Return the sum of all the distances of a node."
           (is (= 1 (farness {:1 #{:2}} :1)))
           (is (= 2 (farness {:1 #{:2 :3}} :1)))))

(deftest closeness-test
  (testing "Returns inverse of the farness of a node."
           (is (= 1 (closeness {:1 #{:2}} :1)))
           (is (= 1/2 (closeness {:1 #{:2 :3}} :1)))))


(deftest closeness-graph-test
  (testing "Return a map with every node and its closeness 
            centrality."
           (is (= {:1 1/2 :2 1/3 :4 1} 
                 (closeness-graph {:1 #{:2 :3} :2 #{:1} :4 #{:5}})))
           (is (= {:1 1/2 :2 1/3 :3 1/3} 
                 (closeness-graph {:1 #{:2 :3} :2 #{:1} :3 #{:1}})))))

(run-tests)