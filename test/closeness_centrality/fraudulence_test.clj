(ns closeness-centrality.fraudulence_test
    (:require [closeness-centrality.fraudulence :refer :all]
              [closeness-centrality.centrality :as c]
              [clojure.test :refer :all]))

(def graph1 {:1 #{:2} :2 #{:1}})
(def graph2 {:1 #{:2} :2 #{:1 :3} :3 #{:2}})

(deftest f-factor-test
  (testing "Calculates the f coefficient:
           f(k) = 1 - (1/2)^k"
           (is (= 1/2 (f-factor 1)))
           (is (= 3/4 (f-factor 2)))))

(deftest mark-fraud-test
  (testing "Mark a node as fraudulent by setting its score to zero, setting 
           the score of the nodes directly affected by the fraudulent to its 
           half and applying the f-factor to the undirect affected's score."
           (is (= {:1 0 :2 1/2} 
                  (mark-fraud graph1 (c/closeness-graph graph1) :1)))
           (is (= {:1 0 :2 0}
                  (mark-fraud graph1 
                              (mark-fraud graph1 (c/closeness-graph graph1) :1)
                              :2)))
           (is (= {:1 0 :2 1/4 :3 3/4} 
                  (mark-fraud graph2 (c/closeness-graph graph2) :1)))))

(deftest calc-scores-test
  (testing "Calculate the updated the scores from a given map of scores
           a sequence of fraudulent nodes."
           (is (= {:1 0 :2 1/2} 
                  (calc-scores graph1 #{:1})))
           (is (= {:1 0 :2 0 :3 3/8} 
                  (calc-scores graph2 #{:1 :2})))))

(run-tests)