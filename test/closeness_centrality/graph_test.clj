(ns closeness-centrality.graph-test
  (:require [clojure.test :refer :all]
            [closeness-centrality.graph :refer :all]))

(def test-file "resources/test-file.txt")

(def test-file-content "1 2
2 3
3 1")

(spit test-file test-file-content)

(def vector-test-file [[:1 :2] [:2 :3] [:3 :1]])

(deftest read-edges-test
  (testing "Get a sequence of edges as pairs of keywords."
        (is (=  vector-test-file (read-edges test-file)))))

(deftest add-directed-edge-test
  (testing "Add a directed edge between n1 and n2 into a graph."
           (is (= {:1 #{:2}} 
                  (add-directed-edge {} :1 :2)))
           (is (= {:1 #{:2} :3 #{:4}} 
                  (add-directed-edge {:1 #{:2}} :3 :4)))))

(deftest add-undirected-edge-test
  (testing "Add a undirected edge by adding a directed edge to
            both directions."
           (is (= {:1 #{:2} :2 #{:1}} 
                  (add-undirected-edge {} [:1 :2])))
           (is (= {:1 #{:2 :3} :2 #{:1} :3 #{:1}}
                  (add-undirected-edge {:1 #{:2} :2 #{:1}} [:1 :3])))))

(deftest generate-graph-test
  (testing "Generate an undirected and unweighted graph from
           a list of edges."
           (is (= {:1 #{:2 :3} :2 #{:1 :3} :3 #{:1 :2}} 
                  (generate-graph (sorted-map) 
                                  (read-edges test-file))))))

(deftest breadth-first-search-test
  (testing "Execute a breadth first search to find the distance
           from the node start to all other nodes."
           (is (= {:2 1 :1 0} 
                  (breadth-first-search {:1 #{:2} :2 #{:1}} :1)))
           (is (= {:3 2 :2 1 :1 0} 
                  (breadth-first-search {:1 #{:2} :2 #{:1 :3}} :1)))))

(run-tests)