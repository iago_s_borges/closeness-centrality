(ns closeness-centrality.fraudulence
  "This namespace provides functions to deal with fraudulence
  among costumers."
  (:require [closeness-centrality.graph :as g]
            [closeness-centrality.centrality :as c]
            [clojure.math.numeric-tower :as math]
            [closeness-centrality.util :as util]))
            
(defn f-factor [k] (- 1 (math/expt 1/2 k)))

(defn mark-fraud
  "Marks a node as fraudulent by setting its score to zero, setting 
  the score of the nodes directly affected by the fraudulent to its 
  half and applying the f-factor to the undirectly affected's score."
  [graph current-scores node]
  {:pre [(contains? graph node)]}
  (let [new-scores {node 0}
        distances (g/breadth-first-search graph node)]
    (reduce-kv (fn [scores-map vertice score]
                 (let [distance (distances vertice)]
                   (if (-> score zero? not)
                     (if (= distance 1)
                       (assoc scores-map vertice (/ score 2))
                       (assoc scores-map vertice (f-factor distance)))
                     (assoc scores-map vertice 0))))
       new-scores current-scores)))

(defn calc-scores
  "Calculates the updated the scores from a given map of scores
  and sequence of fraudulent nodes."
  [graph fraud-nodes]
  (reduce #(mark-fraud graph %1 %2)
          (c/closeness-graph graph)
          fraud-nodes))