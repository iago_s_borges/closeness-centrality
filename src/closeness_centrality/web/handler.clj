(ns closeness-centrality.web.handler
    (:require [ring.util.http-response :refer :all]
              [compojure.api.sweet :refer :all]
              [closeness-centrality.web.service :as service]
              [schema.core :as s]))

(s/defschema Message
  {:message s/Str})

(def app
  (api
    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Closeness Centrality"
                    :description "RESTful web server to provide centrality metrics functionalities for social network analysis."}
             :tags [{:name "api", :description "closeness-centrality"}]}}}
  
   (context "/api" []
      :tags ["api"]
      
      (GET "/rank-vertices" []
        :query-params []
        :summary "Displays a rank of vertices ordered by its scores"
        (ok (service/rank-vertices)))
      
      (PUT "/add-edge/:n1/:n2" []
        :return       Message
        :path-params  [n1 :- s/Int, n2 :- s/Int]
        :summary      "Adds an edge between vertices n1 and n2"
        (ok (service/add-edge @service/graph-usage n1 n2)))
      
      (PUT "/mark-fraud/:node" []
        :return       Message
        :path-params  [node :- s/Int]
        :summary      "Flags a vertice as flaudulent"
        (ok (service/mark-fraudulent @service/graph-usage @service/fraudulent-nodes node))))))