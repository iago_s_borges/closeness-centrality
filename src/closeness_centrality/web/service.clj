(ns closeness-centrality.web.service
  "Service to provide the functionalities from the core namespaces 
   to the web server, and keep its state during runtime."
  (:require [closeness-centrality.graph :as g]
            [closeness-centrality.centrality :as c]
            [closeness-centrality.fraudulence :as f]
            [closeness-centrality.util :as util]))

(def graph-file "resources/edges.txt")

(def fraudulent-nodes (atom #{}))

(def graph-usage (atom (g/generate-graph
                         (sorted-map)(g/read-edges graph-file))))

(defn update-scores [graph f-nodes]
  (util/sort-map-desc (f/calc-scores graph f-nodes)))

(defn add-edge
  "Adds an undirected edge to 'graph-usage'."
  [graph n1 n2]
  (let [v1 (util/int->keyword n1)
        v2 (util/int->keyword n2)]
    {:pre [(contains? graph v1)
           (contains? graph v2)]}
    (swap! graph-usage
           g/add-undirected-edge [v1 v2])
    (update-scores graph @fraudulent-nodes)
  {:message (str "An edge between " n1 " and " n2 " has been added")}))

(defn mark-fraudulent
  "Update the 'scores' to the scores after the node was
  tagged as flaudulent."
  [graph f-nodes node]
  (let [n (util/int->keyword node)]
    {:pre [(contains? graph n)]}
    (reset! fraudulent-nodes (conj f-nodes n))
    (update-scores graph f-nodes)
  {:message (str "The vertex " node " has been flagged as fraudulent")}))

(defn rank-vertices
  "Returns a vector with maps containing a node and its score.
  It returns a vector instead of the pure map because Swagger orders maps 
  by itself, not keeping the natural order."
  []
  (reduce-kv #(conj %1 {%2 %3}) [] 
             (update-scores @graph-usage @fraudulent-nodes)))