(ns closeness-centrality.util
  "This namespace provides generic functions to help other
  functions to do simple tasks."
  (:require [clojure.java.io :as io]))

(defn read-file
  "Returns a vector with the lines of a file."
  [file]
  (with-open [file-content (io/reader file)]
      (vec (line-seq file-content))))

(defn sort-map-desc
  "Returns the map m sorted by its values descending."
  [m]
  (into (sorted-map-by (fn [key1 key2] 
                         (compare [(m key2) key2]
                                  [(m key1) key1]))) m))

(defn int->keyword [int] (keyword (str int)))