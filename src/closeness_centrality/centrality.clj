(ns closeness-centrality.centrality
  "This namespace provides functions to do centrality metrics
  calculations like farness and closeness centrality."
  (:require [closeness-centrality.graph :as graph]
            [clojure.math.numeric-tower :as math]))

(defn farness
  "Returns the sum of all the distances to a node."
  [graph node]
  (let [distances (vals (graph/breadth-first-search graph node))]
    (reduce + distances)))

(defn closeness
  "Returns inverse of the farness of a node."
  [graph node]
  (/ 1 (farness graph node)))

(defn closeness-graph
  "Returns a map with every node from a graph and its 
  closeness to the other nodes."
  [graph]
  (let [nodes (keys graph)]
    (apply merge {}
           (map #(assoc {} % (closeness graph %)) nodes))))