# Closeness Centrality

A RESTful web server to provide closeness centrality functionalities for social network analysis.

## Challenge

In this challenge, suppose we are looking to do social network analysis for prospective customers. We want to extract from their social network a metric called "closeness centrality".

Centrality metrics try to approximate a measure of influence of an individual within a social network. The distance between any two vertices is their shortest path. The *farness* of a given vertex *v* is the sum of all distances from each vertex to *v*. Finally, the *closeness* of a vertex *v* is the inverse of the *farness*.

The first part of the challenge is to rank the vertices in a given undirected graph by their *closeness*. The graph is provided in the attached file; each line of the file consists of two vertex names separated by a single space, representing an edge between those two nodes.

The second part of the challenge is to create a RESTful web server with endpoints to register edges and to render a ranking of vertexes sorted by centrality. We can think of the centrality value for a node as an initial "score" for that customer.

The third and final part is to add another endpoint to flag a customer node as "fraudulent". It should take a vertex id, and update the internal customer score as such:
- The fraudulent customer score should be zero.
- Customers directly related to the "fraudulent" customer should have their score halved.
- More generally, scores of customers indirectly referred by the "fraudulent" customer should be multiplied by a coefficient F:

F(k) = (1 - (1/2)^k)


where k is the length of the shortest path from the "fraudulent" customer to the customer in question.


## Solution

Given the description above, the graph in matter is **undirected** and **unweighted**. Once the graph is unweighted, the choosen algorithm to travel through the graph was the **Breadth-first search** algorithm.
The API used to implement the web server was the Compojure API (https://github.com/metosin/compojure-api).


## Usage

#### Requirements

1. JDK (http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html)
2. Leiningen (http://leiningen.org)

#### Running

1. Clone the repository from https://bitbucket.org/iago_s_borges/closeness-centrality.
2. Once into the repository folder, type the following command: **lein ring server**
3. The page *http://localhost:3000/index.html* will automatically open in the browser with the available endpoints.

#### Tests

To run all the tests, get into the repository folder and type the following command: **lein test**

#### Endpoints

* **add-edge/:n1/:n2** - Adds an edge between the vertex n1 and n2. Returns a success message if successful.
* **mark-fraud/:node** - Flags a node as fraudulent. Returns a success message if successful.
* **rank-vertices** - Returns a vector with pairs of nodes and its scores ranked by their centrality value.


## License

Distributed under the Eclipse Public License either version 1.0 or any later version.s