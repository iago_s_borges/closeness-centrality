(defproject closeness-centrality "0.1.0-SNAPSHOT"
  :ring {:handler closeness-centrality.web.handler/app}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-time "0.11.0"]
                 [metosin/compojure-api "1.0.0"]
                 [org.clojure/math.numeric-tower "0.0.2"]]
  :uberjar-name "server.jar"
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]]
                   :plugins [[lein-ring "0.9.7"]]}})
