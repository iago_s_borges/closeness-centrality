(ns closeness-centrality.graph
  "This namespace provides functions that do generic tasks
  involving graphs, like graph generation and a breadth-first-search
  implementation."
  (:require [closeness-centrality.util :as util]
            [clojure.string :as str]))

(defn read-edges
  "Returns a sequence of edges as pairs of keywords."
  [file]
  (map #(map keyword (str/split % #" "))
       (util/read-file file)))

(defn add-directed-edge
  "Adds a directed edge between n1 and n2 into a graph."
  [graph n1 n2]
  (let [n1-neighbors (n1 graph)]
    (assoc graph n1 (set (conj n1-neighbors n2)))))

(defn add-undirected-edge
  "Adds a undirected edge by adding a directed edge to
  both directions."
  [graph edge]
  (let [[n1 n2] edge]
    (add-directed-edge
     (add-directed-edge graph n1 n2) n2 n1)))

(defn generate-graph
  "Generates an undirected and unweighted graph from
  a list of edges."
  [graph edges]
  (reduce add-undirected-edge graph edges))

(defn associate-distances
  "Calculate the distances from node 'current' to its neighbors."
  [m current new-neighbors]
  (apply merge m (map #(assoc {} %
                              (inc (current m))) new-neighbors)))
                        
(defn breadth-first-search
  "Executes a breadth first search to a graph to obtain
  the result of function f to 'return'. If a function and
  a return type are not informed it calculates the shortest path."
  ([graph start return f]
  (loop [return return
         explored #{start}
         frontier (conj (clojure.lang.PersistentQueue/EMPTY) start)]
    (if (seq frontier)
      (let [current (peek frontier)
            neighbors (current graph)
            new-neighbors (remove explored neighbors)]
        (recur
          (f return current new-neighbors)
          (into explored neighbors)
          (into (pop frontier) new-neighbors)))
       return)))
  ([graph start]
    (breadth-first-search graph start {start 0} associate-distances)))