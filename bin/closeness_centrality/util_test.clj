(ns closeness-centrality.util-test
  (:require [clojure.test :refer :all]
            [closeness-centrality.util :refer :all]))

(def test-file "resources/edges.txt")
  
(deftest read-file-test
  (testing "Read lines from a file."
        (is (= 945 (count (read-file test-file))))))

(deftest sort-map-test
  (testing "Return the map m sorted by values descending."
           (is (= {:3 3 :2 2 :1 1} 
                  (sort-map-desc {:2 2 :3 3 :1 1})))))

(deftest int->keyword-test
  (testing "Return a keyword from a int."
           (is (= :1 (int->keyword 1)))))
 
(run-tests)